# XT pico configuration for BPM RFFE


    =========================== INTERFACE MENU ==================================

      1 = I2C1 Menu
      2 = SPI2 Menu
      E = ETHERNET Menu

















      For example:'A'[ENTER]
    -----------------------------------------------------------------------------
    [Q = QUIT] Please enter your choice:

## PORT1

    =========================== I2C CONFIG MENU =================================

      1 = Slave Addr        = 0
      2 = Baudrate          = 100000
      3 = Data Control      = P
      4 = Data ready timeout= 10(*10ms)

      5 = Flow Control      = N
      6 = RTS Protocol      = 0

      a = Emulation         = TCPSERVER
      b = EmuCode           = 0000
      c = BUS               = I2C
      d = InputTimeOut*10ms = 0
      e = Local Port        = 1002
      f = With SSL/TLS      = N


      STATE=HW ONLINE

      RTS = LOW   CTS = LOW 

      For example:'1=2'
    -----------------------------------------------------------------------------
    [Q = QUIT] Please enter your choice:

## PORT2

    =========================== SPI CONFIG MENU =================================

      1 = Master/Slave      = M
      2 = Bitrate           = 125000(125.000)
      3 = Databits          = 24
      4 = CPOL              = 0
      5 = CPHA              = 0
      6 = Data Control      = N
      7 = Data Poll*10ms    = 10
      8 = Flow Control      = N
      9 = RTS Protocol      = 0
      a = CS Control        = S

      b = Emulation         = TCPSERVER
      c = EmuCode           = 0000
      d = BUS               = SPI
      e = InputTimeOut*10ms = 0
      f = Local Port        = 1003
      g = With SSL/TLS      = N

      RTS = LOW   CTS = LOW   STATE=HW ONLINE

      For example:'1=M'
    -----------------------------------------------------------------------------
    [Q = QUIT] Please enter your choice:
